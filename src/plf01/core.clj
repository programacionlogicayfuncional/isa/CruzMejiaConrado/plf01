(ns plf01.core)

; 1
(defn function-<-1
  [a b] (< a b))

(defn function-<-2
  [a b c] (< a b c))

(defn function-<-3
  [a b c d] (< a b c d))

; 2
(defn function-<=-1
  [a b] (<= a b))

(defn function-<=-2
  [a b c] (<= a b c))

(defn function-<=-3
  [a b c d] (<= a b c d))

; 3
(defn function-==-1
  [a b] (== a b))

(defn function-==-2
  [a b c] (== a b c))

(defn function-==-3
  [a b c d] (== a b c d))

; 4
(defn function->-1
  [a b] (> a b))

(defn function->-2
  [a b c] (> a b c))

(defn function->-3
  [a b c d] (> a b c d))

; 5
(defn function->=-1
  [a b] (>= a b))

(defn function->=-2
  [a b c] (>= a b c))

(defn function->=-3
  [a b c d] (>= a b c d))

; 6
(defn function-assoc-1
  [a] (assoc a 0 "hola"))

(defn function-assoc-2
  [a b] (assoc a b "hola"))

(defn function-assoc-3
  [a b c] (assoc a b c))

;7
(defn function-assoc-in-1
  [a b c] (assoc-in a b c))

(defn function-assoc-in-2
  [a b c] (assoc-in a b c))

(defn function-assoc-in-3
  [a b c] (assoc-in a b c))

;8
(defn function-concat-1
   [a] (concat a))

(defn function-concat-2
  [a b] (concat a b))

(defn function-concat-3
  [a b c] (concat a b c))

;9 
(defn function-conj-1
  [a] (conj a))

(defn function-conj-2
  [a b] (conj a b))

(defn function-conj-3
  [a b c] (conj a b c))

;10
(defn function-cons-1
  [a b] (cons a b))

(defn function-cons-2
  [a b] (cons a b))

(defn function-cons-3
  [a b] (cons a b))

;11
(defn function-contains?-1
  [a b] (contains? a b))

(defn function-contains?-2
  [a b] (contains? a b))

(defn function-contains?-3
  [a b] (contains? a b))

;12
(defn function-count-1
  [a] (count a))

(defn function-count-2
  [a] (count a))

(defn function-count-3
  [a] (count a))

;13
(defn function-disj-1
  [a] (disj a)) 

(defn function-disj-2
  [a b] (disj a b)) 

(defn function-disj-3
  [a b c] (disj a b c))

;14
(defn function-dissoc-1
  [a] (dissoc a))

(defn function-dissoc-2
  [a b] (dissoc a b))

(defn function-dissoc-3
  [a b c] (dissoc a b c))


;15
(defn function-pop-1
  [a] (pop a))

(defn function-pop-2
  [a] (pop a)) 

(defn function-pop-3
  [a] (pop a)) 


;16
(defn function-distinct-1
  [a] (distinct a))

(defn function-distinct-2
  [a] (distinct a))

(defn function-distinct-3
  [a] (distinct a))


;17
(defn function-distinct?-1
  [a] (distinct? a))

(defn function-distinct?-2
  [a b] (distinct? a b))

(defn function-distinct?-3
  [a b c] (distinct? a b c))

;18
(defn function-drop-last-1
  [a] (drop-last a))

(defn function-drop-last-2
  [a b] (drop-last a b))

(defn function-drop-last-3
  [a b] (drop-last a b))

;19
(defn function-empty-1
  [a] (empty a))

(defn function-empty-2
  [a] (empty a))

(defn function-empty-3
  [a] (empty a))


;20
(defn function-empty?-1
  [a] (empty? a))

(defn function-empty?-2
  [a] (empty? a))

(defn function-empty?-3
  [a] (empty? a))


;21
(defn function-even?-1
  [a] (even? a))

(defn function-even?-2
  [a] (even? a))

(defn function-even?-3
  [a] (even? a))

;22
(defn function-false?-1
  [a] (false? a))

(defn function-false?-2
  [a] (false? a))

(defn function-false?-3
  [a] (false? a))

;23
(defn function-find-1
  [a b] (find a b))

(defn function-find-2
  [a b] (find a b))

(defn function-find-3
  [a b] (find a b))


;24
(defn function-first-1
  [a] (first a))

(defn function-first-2
  [a] (first a))

(defn function-first-3
  [a] (first a))

;25
(defn function-flatten-1
  [a] (flatten a))

(defn function-flatten-2
  [a] (flatten a))

(defn function-flatten-3
  [a] (flatten a))
  
;26
(defn function-frequencies-1
  [a] (frequencies a))

(defn function-frequencies-2
  [a] (frequencies a))

(defn function-frequencies-3
  [a] (frequencies a))


;27
(defn function-get-1
  [a b] (get a b))

(defn function-get-2
  [a b] (get a b))

(defn function-get-3
  [a b c] (get a b c))

;28
(defn function-get-in-1
  [a b] (get-in a b))

(defn function-get-in-2
  [a b] (get-in a b))

(defn function-get-in-3
  [a b c] (get-in a b c))


;29
(defn function-into-1
  [a b] (into a b))

(defn function-into-2
  [a b] (into a b))

(defn function-into-3
  [a b] (into a b))

;30
(defn function-key-1
  [a] (map key a))

(defn function-key-2
  [a] (map key a))

(defn function-key-3
  [a] (map key a))

;31
(defn function-keys-1
  [a] (keys a))

(defn function-keys-2
  [a] (keys a))

(defn function-keys-3
  [a] (keys a))


;32
(defn function-max-1
  [a] (max a))

(defn function-max-2
  [a b] (max a b))

(defn function-max-3
  [a b c] (max a b c))

;33
(defn function-min-1
  [a] (min a))

(defn function-min-2
  [a b] (min a b))

(defn function-min-3
  [a b c] (min a b c))


;34
(defn function-merge-1
  [a b] (merge a b))

(defn function-merge-2
  [a b] (merge a b))

(defn function-merge-3
  [a b] (merge a b))

;35
(defn function-neg?-1
  [a] (neg? a))

(defn function-neg?-1
  [a] (neg? a))

(defn function-neg?-1
  [a] (neg? a))

;36
(defn function-nil?-1
  [a] (nil? a))

(defn function-nil?-2
  [a] (nil? a))

(defn function-nil?-3
  [a] (nil? a))


;37
(defn function-not-empty-1
   [a] (not-empty a))

(defn function-not-empty-2
  [a] (not-empty a))

(defn function-not-empty-3
  [a] (not-empty a))

;38
(defn function-nth-1
  [a b] (nth a b))

(defn function-nth-2
  [a b] (nth a b))

(defn function-nth-3
  [a b c] (nth a b c))


;39
(defn function-odd?-1
  [a] (odd? a))

(defn function-odd?-2
  [a] (odd? a))

(defn function-odd?-3
  [a] (odd? a))

;40
(defn function-partition-1
  [a b] (partition a b))

(defn function-partition-2
  [a b c] (partition a b c))

(defn function-partition-3
  [a b c d] (partition a b c d))

;41
(defn function-partition-all-1
  [a b] (partition-all a b))

(defn function-partition-all-2
  [a b c] (partition-all a b c))

(defn function-partition-all-3
  [a b c] (partition-all a b c))

;42
(defn function-peek-1
  [a] (peek a))

(defn function-peek-2
  [a] (peek a))

(defn function-peek-3
  [a] (peek a))

;43
(defn function-pos?-1
  [a] (pos? a))

(defn function-pos?-2
  [a] (pos? a))

(defn function-pos?-3
  [a] (pos? a))


;44
(defn function-quot-1
   [a b] (quot a b))

(defn function-quot-2
  [a b] (quot a b))

(defn function-quot-3
  [a b] (quot a b))

;45
(defn function-range-1 
  [] (range))

(defn function-range-2 
  [a] (range a))

(defn function-range-3
  [a b] (range a b))

;46

(defn function-rem-1
  [a b] (rem a b))

(defn function-rem-2
  [a b] (rem a b))

(defn function-rem-3
  [a b] (rem a b))

;47
(defn function-repeat-1
  [a] (take 5 (repeat a)))

(defn function-repeat-2
  [a b] (repeat a b))

(defn function-repeat-3
  [a b] (repeat a b))

;48
(defn function-replace-1
  [a b] (replace a b))

(defn function-replace-2
  [a b] (replace a b))

(defn function-replace-3
  [a b] (replace a b))

;49
(defn function-rest-1
  [a] (rest a))

(defn function-rest-2
  [a] (rest a))

(defn function-rest-3
  [a] (rest a))

;50
(defn function-select-keys-1
  [a b] (select-keys a b))

(defn function-select-keys-2
  [a b] (select-keys a b))

(defn function-select-keys-3
  [a b] (select-keys a b))

;51
(defn function-shuffle-1
  [a] (shuffle a))

(defn function-shuffle-2
  [a] (shuffle a))

(defn function-shuffle-3
  [a] (shuffle a))


;52
(defn function-sort-1
  [a] (sort a))

(defn function-sort-2
  [a b] (sort a b))

(defn function-sort-3
  [a b] (sort a b))

;53
(defn function-split-at-1
   [a b] (split-at a b))

(defn function-split-at-2
  [a b] (split-at a b))

(defn function-split-at-3
  [a b] (split-at a b))

;54
(defn function-str-1
  [] (str))

(defn function-str-2
  [a] (str a))

(defn function-str-3
  [a b c] (str a b c))

;55
(defn function-subs-1
  [a b] (subs a b))

(defn function-subs-2
  [a b c] (subs a b c))

(defn function-subs-3
[a b c] (subs a b c))

;56
(defn function-subvec-1
  [a b] (subvec a b))

(defn function-subvec-2
  [a b c] (subvec a b c))

(defn function-subvec-3
  [a b c] (subvec a b c))

;57
(defn function-take-1
  [a] (take a))

(defn function-take-2
  [a b] (take a b))

(defn function-take-3
  [a b] (take a b))

;58
(defn function-true?-1
  [a] (true? a))

(defn function-true?-2
  [a] (true? a))

(defn function-true?-3
  [a] (true? a))

;59

(defn function-val-1
  [a] (map val a))

(defn function-val-2
  [a] (map val a))

(defn function-val-3
  [a] (map val a))


;60
(defn function-vals-1
  [a] (vals a))

(defn function-vals-2
  [a] (vals a))

(defn function-vals-3
  [a] (vals a))

;61
(defn function-zero?-1
  [a] (zero? a))

(defn function-zero?-2
  [a] (zero? a))

(defn function-zero?-3
  [a] (zero? a))

;62
(defn function-zipmap-1
  [a b] (zipmap a b))

(defn function-zipmap-2
  [a b] (zipmap a b))

(defn function-zipmap-3
  [a b] (zipmap a b))

;completed